import React from 'react'
import {Route, Switch} from 'react-router-dom'
import MainPage from '../components/MainPage'
import Login from '../components/Login'

export const Router = () => {
    return (
        <React.Fragment>
            <main className="content">
                <div className="container">
                    <Switch>
                        <Route exact path="/" component={MainPage}/>
                        <Route exact path="/login" component={Login}/>
                    </Switch>
                </div>
            </main>
        </React.Fragment>
    )
};
