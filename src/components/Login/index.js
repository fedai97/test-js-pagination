import React, {useState} from 'react'
import {
    Avatar,
    CssBaseline,
    Typography,
    Container,
} from '@material-ui/core'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import {useStyles} from './style'
import LoginForm from './LoginForm'
import AuthAPI from '../../services/api-worker/AuthApi'

export default () => {
    const classes = useStyles();

    const [submitted, setStateSubmitted] = useState(false);
    const [formData, setStateFormData] = useState({'username': '', 'password': ''});

    const handleInputChange = event => {
        const {value, name} = event.target;
        setStateFormData({...formData, ...{[name]: value}});
    };

    const onSubmit = event => {
        event.preventDefault();
        setStateSubmitted(true);
        new AuthAPI()
            .login(formData)
            .then(res => {
                if (res.status === 'ok') {
                    sessionStorage.setItem('token', res.message.token);
                    window.location.href = '/';
                } else {
                    res.message.username ?  alert(res.message.username) :  alert(res.message.password);
                    setStateSubmitted(false);
                    setStateFormData(prevState => {
                        return {...prevState, ...{password: ''}}
                    });
                }
            })
    };

    return (
        <>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <LoginForm
                        onSubmit={onSubmit}
                        handleInputChange={handleInputChange}
                        submitted={submitted}
                        formData={formData}
                    />
                </div>
            </Container>
        </>
    )
}
