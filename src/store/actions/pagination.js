import * as PAGINATION from "../constants/pagination";

export function getPagination(count) {
    return function (dispatch) {
        return dispatch({
            type: PAGINATION.PAGINATION_GET,
            data: count
        });
    };
}

export function addPagination() {
    return function (dispatch) {
        return dispatch({
            type: PAGINATION.PAGINATION_ADD,
        });
    };
}
