import * as PAGINATION from '../constants/pagination';

const initState = {
	count: 0,
};

export default function (state = initState, action) {
	switch (action.type) {
		case PAGINATION.PAGINATION_GET:
			return {
				...state,
				...{
					count: action.data,
				}
			};


		case PAGINATION.PAGINATION_ADD:
			return {
				...state,
				...{
					count : +state.count + 1,
				}
			};

		default:
			return state
	}
}
